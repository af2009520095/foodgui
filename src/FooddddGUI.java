import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FooddddGUI {
    public static int a=0;
    void order(String food,int kk){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if (confirmation == 0) {
            JOptionPane.showMessageDialog(null, "Thank you for ordering! It will be served" +
                    " as soon as possible."
            );
            String currentText = textPane1.getText();
            textPane1.setText(currentText + food + " " + kk + "yen" + "\n");
            a += kk;
            abc.setText("Total  " + a + "yen");
        }

    }
    private JPanel root;
    private JButton templaButton;
    private JButton udonButton;
    private JButton ramenButton;
    private JTextPane textPane1;
    private JButton takanaButton;
    private JButton saradaButton;
    private JButton imoButton;
    private JButton button1;
    private JLabel abc;

    public static void main(String[] args) {
        JFrame frame = new JFrame("FooddddGUI");
        frame.setContentPane(new FooddddGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }

    public FooddddGUI(){
        templaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Templa",100);

            }

        });
        templaButton.setIcon(new ImageIcon( this.getClass().getResource("tempura.jpg")));;
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen",100);

            }

        });
        ramenButton.setIcon(new ImageIcon( this.getClass().getResource("ramen.jpg")));;

        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon",100);

            }
        });
        udonButton.setIcon(new ImageIcon( this.getClass().getResource("udon.jpg")));;

        takanaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("takana",100);

            }
        });
        takanaButton.setIcon(new ImageIcon( this.getClass().getResource("takana.jpg")));;
        imoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("imo",100);

            }
        });
        imoButton.setIcon(new ImageIcon( this.getClass().getResource("imo.png")));;
        saradaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("sarada",100);

            }
        });
        saradaButton.setIcon(new ImageIcon( this.getClass().getResource("sarada.jpg")));;
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "would you like to checkout? ",
                        "Order confimaiton",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation==0) {
                    textPane1.setText(null);
                    JOptionPane.showMessageDialog(null, "Thank you .The total price is"
                            + a + " yen.");
                    a=0;
                    abc.setText("Total  "+a+"yen");
                }


            }
        });
            }
}
